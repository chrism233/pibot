﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GetPanTiltPosResponse.cs" company="">
//   
// </copyright>
// <summary>
//   Represents a GetPanTiltPosResponse message.
//   This message type will be sent form the device to the remote terminal.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace PiBot.Common.Commands
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    using CmdMessenger.Commands;

    /// <summary>
    ///     Represents a GetPanTiltPosResponse message.
    ///     This message type will be sent form the device to the remote terminal.
    /// </summary>
    public class GetPanTiltPosResponse : ReceivedCommand, IEquatable<GetPanTiltPosResponse>
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="GetPanTiltPosResponse"/> class.
        /// </summary>
        /// <param name="commandId">
        /// The command Id.
        /// </param>
        /// <param name="arguments">
        /// The command arguments.
        /// </param>
        public GetPanTiltPosResponse(int commandId, string[] arguments)
            : base(commandId, arguments)
        {
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other"/> parameter; otherwise, false.
        /// </returns>
        /// <param name="other">
        /// An object to compare with this object.
        /// </param>
        [SuppressMessage("Microsoft.Design", "CA1065:DoNotRaiseExceptionsInUnexpectedLocations", 
            Justification = "Not implemented yet.")]
        public bool Equals(GetPanTiltPosResponse other)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}