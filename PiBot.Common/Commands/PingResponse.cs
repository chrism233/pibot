﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PingResponse.cs" company="">
//   
// </copyright>
// <summary>
//   The ping response.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace PiBot.Common.Commands
{
    using CmdMessenger.Commands;

    /// <summary>
    ///     The ping response.
    /// </summary>
    public class PingResponse : SendCommand
    {
        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="PingResponse" /> class.
        /// </summary>
        public PingResponse()
            : base((int)PiBotCommands.PingResponse)
        {
        }

        #endregion
    }
}