﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GetSensorValueResponse.cs" company="">
//   
// </copyright>
// <summary>
//   Represents a GetSensorValueResponse message.
//   This message type is designed to be sent from the device to the remote terminal.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace PiBot.Common.Commands
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    using CmdMessenger.Commands;

    /// <summary>
    ///     Represents a GetSensorValueResponse message.
    ///     This message type is designed to be sent from the device to the remote terminal.
    /// </summary>
    public class GetSensorValueResponse : ReceivedCommand, IEquatable<GetSensorValueResponse>
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="GetSensorValueResponse"/> class.
        /// </summary>
        /// <param name="commandId">
        /// The command Id.
        /// </param>
        /// <param name="arguments">
        /// The arguments.
        /// </param>
        public GetSensorValueResponse(int commandId, string[] arguments)
            : base(commandId, arguments)
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the unique identifier of the sensor.
        /// </summary>
        public int SensorId { get; private set; }

        /// <summary>
        ///     Gets the current value of the sensor.
        /// </summary>
        public int SensorValue { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other"/> parameter; otherwise, false.
        /// </returns>
        /// <param name="other">
        /// An object to compare with this object.
        /// </param>
        [SuppressMessage("Microsoft.Design", "CA1065:DoNotRaiseExceptionsInUnexpectedLocations", 
            Justification = "Not implemented yet.")]
        public bool Equals(GetSensorValueResponse other)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}