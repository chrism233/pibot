﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Ping.cs" company="">
//   
// </copyright>
// <summary>
//   The ping.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace PiBot.Common.Commands
{
    using CmdMessenger.Commands;

    /// <summary>
    ///     The ping.
    /// </summary>
    public class Ping : SendCommand
    {
        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="Ping" /> class.
        /// </summary>
        public Ping()
            : base((int)PiBotCommands.Ping, (int)PiBotCommands.PingResponse)
        {
        }

        #endregion
    }
}