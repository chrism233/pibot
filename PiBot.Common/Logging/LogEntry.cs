﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LogEntry.cs" company="">
//   
// </copyright>
// <summary>
//   The log entry.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace PiBot.Common.Logging
{
    /// <summary>
    ///     The log entry.
    /// </summary>
    public class LogEntry
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LogEntry"/> class.
        /// </summary>
        /// <param name="logLevel">
        /// The log level.
        /// </param>
        /// <param name="message">
        /// The message.
        /// </param>
        public LogEntry(LogLevel logLevel, string message)
        {
            this.LogLevel = logLevel;
            this.Message = message;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the log level.
        /// </summary>
        public LogLevel LogLevel { get; set; }

        /// <summary>
        ///     Gets or sets the message.
        /// </summary>
        public string Message { get; set; }

        #endregion
    }
}