﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LogLevel.cs" company="">
//   
// </copyright>
// <summary>
//   The log level.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace PiBot.Common.Logging
{
    /// <summary>
    ///     The log level.
    /// </summary>
    public enum LogLevel
    {
        /// <summary>
        ///     Info level log entry.
        /// </summary>
        Info, 

        /// <summary>
        ///     Warn level log entry.
        /// </summary>
        Warn, 

        /// <summary>
        ///     Error level log entry.
        /// </summary>
        Error, 

        /// <summary>
        ///     The input.
        /// </summary>
        Input, 

        /// <summary>
        ///     The output.
        /// </summary>
        Output
    }
}