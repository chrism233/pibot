﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LogVM.cs" company="">
//   
// </copyright>
// <summary>
//   A view-model for log entries.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace PiBot.Common.Logging
{
    using System.Collections.ObjectModel;

    /// <summary>
    ///     A view-model for log entries.
    /// </summary>
    public class LogVM : ILog
    {
        #region Fields

        /// <summary>
        ///     The log entries.
        /// </summary>
        private readonly ObservableCollection<LogEntry> messages = new ObservableCollection<LogEntry>();

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the log entries.
        /// </summary>
        public ObservableCollection<LogEntry> Messages
        {
            get
            {
                return this.messages;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The error.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="obj">
        /// The obj.
        /// </param>
        public void Error(string message, params object[] obj)
        {
            this.messages.Add(new LogEntry(LogLevel.Error, string.Format(message, obj)));
        }

        /// <summary>
        /// The info.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="obj">
        /// The obj.
        /// </param>
        public void Info(string message, params object[] obj)
        {
            this.Messages.Add(new LogEntry(LogLevel.Info, string.Format(message, obj)));
        }

        /// <summary>
        /// The input.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="obj">
        /// The obj.
        /// </param>
        public void Input(string message, params object[] obj)
        {
            this.messages.Add(new LogEntry(LogLevel.Input, string.Format(message, obj)));
        }

        /// <summary>
        /// The output.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="obj">
        /// The obj.
        /// </param>
        public void Output(string message, params object[] obj)
        {
            this.messages.Add(new LogEntry(LogLevel.Output, string.Format(message, obj)));
        }

        /// <summary>
        /// The warn.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="obj">
        /// The obj.
        /// </param>
        public void Warn(string message, params object[] obj)
        {
            this.messages.Add(new LogEntry(LogLevel.Warn, string.Format(message, obj)));
        }

        #endregion
    }
}