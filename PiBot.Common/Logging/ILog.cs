﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ILog.cs" company="">
//   
// </copyright>
// <summary>
//   The Log interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace PiBot.Common.Logging
{
    using System.Collections.ObjectModel;

    /// <summary>
    /// The Log interface.
    /// </summary>
    public interface ILog
    {
        #region Public Properties

        /// <summary>
        ///     Gets the log entries.
        /// </summary>
        ObservableCollection<LogEntry> Messages { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The error.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="obj">
        /// The obj.
        /// </param>
        void Error(string message, params object[] obj);

        /// <summary>
        /// The info.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="obj">
        /// The obj.
        /// </param>
        void Info(string message, params object[] obj);

        /// <summary>
        /// The input.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="obj">
        /// The obj.
        /// </param>
        void Input(string message, params object[] obj);

        /// <summary>
        /// The output.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="obj">
        /// The obj.
        /// </param>
        void Output(string message, params object[] obj);

        /// <summary>
        /// The warn.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="obj">
        /// The obj.
        /// </param>
        void Warn(string message, params object[] obj);

        #endregion
    }
}