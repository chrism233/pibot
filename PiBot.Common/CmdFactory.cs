﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdFactory.cs" company="PiBot">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace PiBot.Common
{
    using PiBot.Common.Commands;

    /// <summary>
    /// A message factory type used to create and parse all message types.
    /// </summary>
    public class CmdFactory
    {
        #region Methods

        /// <summary>
        /// Creates a <see cref="Fault"/> message. 
        /// </summary>
        /// <param name="message">A message describing the fault.</param>
        /// <returns>A <see cref="Fault"/> instance.</returns>
        public Fault CreateFaultMessage(string message)
        {
            // return new Fault(message);
            return null;
        }

        /// <summary>
        /// Create a <see cref="GetMotorSpeed"/> message.
        /// </summary>
        /// <returns>A <see cref="GetMotorSpeed"/> instance.</returns>
        public GetMotorSpeed CreateGetMotorSpeedMessage()
        {
            // return new GetMotorSpeed();
            return null;
        }

        /// <summary>
        /// Create a <see cref="GetMotorSpeedResponse"/> message.
        /// </summary>
        /// <param name="leftSpeedF">front left motor speed.</param>
        /// <param name="rightSpeedF">front right motor speed.</param>
        /// <param name="leftSpeedR">rear left motor speed.</param>
        /// <param name="rightSpeedR">rear right motor speed.</param>
        /// <returns>A <see cref="GetMotorSpeedResponse"/> instance.</returns>
        public GetMotorSpeedResponse CreateGetMotorSpeedResponseMessage(int leftSpeedF, int rightSpeedF, int leftSpeedR, int rightSpeedR)
        {
            // return new GetMotorSpeedResponse(leftSpeedF, rightSpeedF, rightSpeedR, leftSpeedR);
            return null;
        }

        /// <summary>
        /// Create a <see cref="GetPanTiltPos"/> message.
        /// </summary>
        /// <returns>A <see cref="GetPanTiltPos"/> instance.</returns>
        public GetPanTiltPos CreateGetPanTiltPos()
        {
            // return new GetPanTiltPos(ProtocolVersion);
            return null;
        }

        /// <summary>
        /// Create a <see cref="GetPanTiltPosResponse"/> message.
        /// </summary>
        /// <param name="pan">The pan position.</param>
        /// <param name="tilt">The tilt position.</param>
        /// <returns>A <see cref="GetPanTiltPosResponse"/> instance.</returns>
        public GetPanTiltPosResponse CreateGetPanTiltPosResponse(int pan, int tilt)
        {
            // return new GetPanTiltPosResponse(ProtocolVersion, pan, tilt);
            return null;
        }

        /// <summary>
        /// Create a <see cref="GetSensorValue"/> message.
        /// </summary>
        /// <param name="sensorId">The ID of the sensor.</param>
        /// <returns>A <see cref="GetSensorValue"/> instance.</returns>
        public GetSensorValue CreateGetSensorValueMessage(byte sensorId)
        {
            // return new GetSensorValue(ProtocolVersion, sensorId);
            return null;
        }

        /// <summary>
        /// Create a <see cref="GetSensorValueResponse"/> message.
        /// </summary>
        /// <param name="sensorId">The ID or the sensor.</param>
        /// <param name="sensorValue">The sensors value.</param>
        /// <returns>A <see cref="GetSensorValueResponse"/> instance.</returns>
        public GetSensorValueResponse CreateGetSensorValueResponseMessage(byte sensorId, int sensorValue)
        {
            // return new GetSensorValueResponse(ProtocolVersion, sensorId, sensorValue);
            return null;
        }

        /// <summary>
        /// Create a <see cref="GetSensorValue"/> message.
        /// </summary>
        /// <param name="leftSpeedF">Front left motor speed.</param>
        /// <param name="rightSpeedF">Front right motor speed.</param>
        /// <param name="leftSpeedR">Rear left motor speed.</param>
        /// <param name="rightSpeedR">Rear right motor speed.</param>
        /// <returns>A <see cref="SendMotorSpeed"/> instance.</returns>
        public SendMotorSpeed CreateSendMotorSpeedMessage(int leftSpeedF, int rightSpeedF, int leftSpeedR, int rightSpeedR)
        {
            // return new SendMotorSpeed(ProtocolVersion, leftSpeedF, rightSpeedF, rightSpeedR, leftSpeedR);
            return null;
        }

        /// <summary>
        /// Create a <see cref="SendPanTiltPos"/> message.
        /// </summary>
        /// <param name="pan">The pan position.</param>
        /// <param name="tilt">The tilt position.</param>
        /// <returns>A <see cref="SendPanTiltPos"/> instance.</returns>
        public SendPanTiltPos CreateSendPanTiltPosMessage(int pan, int tilt)
        {
            // return new SendPanTiltPos(ProtocolVersion, pan, tilt);
            return null;
        }

        /// <summary>
        /// Create a <see cref="SendSensorValue"/> message.
        /// </summary>
        /// <param name="sensorId">The sensors ID.</param>
        /// <param name="sensorValue">The sensors value.</param>
        /// <returns>A <see cref="SendSensorValue"/> instance.</returns>
        public SendSensorValue CreateSendSensorValueMessage(byte sensorId, byte sensorValue)
        {
            // return new SendSensorValue(ProtocolVersion, sensorId, sensorValue);
            return null;
        }

        /// <summary>
        /// Create a <see cref="SetMotorSpeed"/> message.
        /// </summary>
        /// <param name="leftSpeedF">Front left motor speed.</param>
        /// <param name="rightSpeedF">Front right motor speed.</param>
        /// <param name="leftSpeedR">Rear left motor speed.</param>
        /// <param name="rightSpeedR">Rear right motor speed.</param>
        /// <returns>A<see cref="SetMotorSpeed"/> instance.</returns>
        public SetMotorSpeed CreateSetMotorSpeedMessage(int leftSpeedF, int rightSpeedF, int leftSpeedR, int rightSpeedR)
        {
            return new SetMotorSpeed(leftSpeedF, rightSpeedF, leftSpeedR, rightSpeedR);
        }

        /// <summary>
        /// Create a <see cref="SetPanTiltPos"/> message.
        /// </summary>
        /// <param name="pan">The pan position.</param>
        /// <param name="tilt">The tilt position.</param>
        /// <returns>A <see cref="SetPanTiltPos"/> instance.</returns>
        public SetPanTiltPos CreateSetPanTiltPosMessage(byte pan, byte tilt)
        {
            // return new SetPanTiltPos(ProtocolVersion, pan, tilt);
            return null;
        }

        #endregion
    }
}
