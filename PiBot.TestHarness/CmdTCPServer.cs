﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdServer.cs" company="PiBot">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The cmd server.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Concurrent;

namespace PiBot.TestHarness
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Net.Sockets;
    using System.Threading;
    using System.Threading.Tasks;

    using CmdMessenger;
    using CmdMessenger.Commands;

    using global::CmdMessenger.CmdComms;

    using PiBot.Common.Commands;
    using PiBot.Common.Logging;

    /// <summary>
    /// The cmd server.
    /// </summary>
    public class CmdTcpServer
    {
        #region Fields

        /// <summary>
        /// The clients.
        /// </summary>
        private readonly ConcurrentDictionary<string, CmdMessenger> clients = new ConcurrentDictionary<string, CmdMessenger>();

        /// <summary>
        /// The listeners.
        /// </summary>
        private readonly Dictionary<int, List<ICommandObserver>> listeners = new Dictionary<int, List<ICommandObserver>>();

        /// <summary>
        /// The logger.
        /// </summary>
        private readonly ILog logger;

        /// <summary>
        /// The cts.
        /// </summary>
        private CancellationTokenSource cts;

        /// <summary>
        /// The listener.
        /// </summary>
        private TcpListener listener;

        /// <summary>
        /// The timer.
        /// </summary>
        private Timer timer;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CmdTcpServer"/> class.
        /// </summary>
        /// <param name="port">
        /// The port.
        /// </param>
        /// <param name="logger">
        /// The logger.
        /// </param>
        public CmdTcpServer(ILog logger)
        {
            this.logger = logger;
            this.timer = new Timer(this.CheckClientAvailability);
        }

        #endregion

        #region Events

        /// <summary>
        /// Occurs when a client connects
        /// </summary>
        public event EventHandler<CmdMessenger> ClientConnectedEvent;

        /// <summary>
        /// The client disconnected event.
        /// </summary>
        public event EventHandler ClientDisconnectedEvent;

        #endregion

        #region Methods

        /// <summary>
        /// The register.
        /// </summary>
        /// <param name="commandId">
        /// The set motor speed.
        /// </param>
        /// <param name="commandHandler">
        /// The handle motor speed.
        /// </param>
        public void Register(int commandId, Action<IReceivedCommand> commandHandler)
        {
            Register(commandId, new CommandObserver(commandHandler));
        }

        /// <summary>
        /// The register.
        /// </summary>
        /// <param name="commandId">
        /// The set motor speed.
        /// </param>
        /// <param name="commandObserver">
        /// The handle motor speed.
        /// </param>
        public void Register(int commandId, ICommandObserver commandObserver)
        {
            if (!this.listeners.ContainsKey(commandId))
            {
                this.listeners.Add(commandId, new List<ICommandObserver>());
                this.listeners[commandId].Add(commandObserver);
            }
            else
            {
                this.listeners[commandId].Add(commandObserver);
            }
        }

        /// <summary>
        /// The start.
        /// </summary>
        /// <param name="port"></param>
        public async void Start(int port)
        {
            this.listener = new TcpListener(port);
            this.cts = new CancellationTokenSource();
            this.listener.Start();

            do
            {
                await this.AcceptClient();
            }
            while (!this.cts.IsCancellationRequested);
        }

        /// <summary>
        /// The accept client.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        private async Task AcceptClient()
        {
            TcpClient client = await this.listener.AcceptTcpClientAsync();
            var cmdMessenger = new CmdMessenger(new TcpCmdClient(client));
            cmdMessenger.Start();
            this.OnClientConnected(cmdMessenger);
            this.logger.Info(string.Format("New client connected {0}", client.Client.RemoteEndPoint));
            if (this.clients.TryAdd(client.Client.RemoteEndPoint.ToString(), cmdMessenger))
            {
                SyncListeners();
            }

            this.timer.Change(500, 500);
        }

        private void SyncListeners()
        {
            foreach (var cmdMessenger in this.clients.Values)
            {
                foreach (var l in this.listeners)
                {
                    foreach (var kvp in l.Value)
                    {
                        cmdMessenger.Register(l.Key, kvp);
                    }
                }
            }
        }

        /// <summary>
        /// The stop.
        /// </summary>
        public void Stop()
        {
            this.cts.Cancel();
            this.listener.Stop();
        }
        
        /// <summary>
        /// The on client connected.
        /// </summary>
        /// <param name="cmdMessenger">
        /// The e.
        /// </param>
        protected virtual void OnClientConnected(CmdMessenger cmdMessenger)
        {
            EventHandler<CmdMessenger> handler = this.ClientConnectedEvent;
            if (handler != null)
            {
                handler(this, cmdMessenger);
            }
        }

        /// <summary>
        /// The on client disconnected.
        /// </summary>
        private void OnClientDisconnected()
        {
            EventHandler handler = this.ClientDisconnectedEvent;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// The check client availability.
        /// </summary>
        /// <param name="state">
        /// The state.
        /// </param>
        private async void CheckClientAvailability(object state)
        {
            foreach (var client in this.clients)
            {
                try
                {
                    await client.Value.SendAsync(new Ping());
                }
                catch (TaskCanceledException)
                {
                    CmdMessenger m;
                    if (this.clients.TryRemove(client.Key, out m))
                    {
                        m.Stop();
                        this.OnClientDisconnected();
                    }
                    else
                    {
                        Debug.WriteLine("Failed to remove client.");
                    }
                }
                catch (Exception)
                {
                    CmdMessenger m;
                    if (this.clients.TryRemove(client.Key, out m))
                    {
                        m.Stop();
                        this.OnClientDisconnected();
                    }
                    else
                    {
                        Debug.WriteLine("Failed to remove client.");
                    }
                }
            }
        }

        #endregion
    }
}