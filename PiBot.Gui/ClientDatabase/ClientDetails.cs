﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ClientDetails.cs" company="PiBot">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace PiBot.GUI.ClientDatabase
{
    using System;
    using System.Xml.Linq;

    using CmdMessenger.CmdComms;

    public abstract class ClientDetails
    {
        #region Properties

        public abstract ClientType ClientType { get; }
        
        /// <summary>
        /// Gets a description of the comms;
        /// </summary>
        public string Description
        {
            get
            {
                return this.GetDescription();
            }
        }

        #endregion

        /// <summary>
        /// Get a description of the comms;
        /// </summary>
        /// <returns>
        /// A description of the interface.
        /// </returns>
        protected abstract string GetDescription();

        public abstract ICmdComms Create();

        public static ClientDetails Parse(XElement element)
        {
            var type = (ClientType)Enum.Parse(typeof(ClientType), element.Attribute("type").Value, true);

            switch (type)
            {
                case ClientType.Tcp:
                    return new TcpClientDetails(element);
                case ClientType.Serial:
                    return new SerialClientDetails(element);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

    }
}