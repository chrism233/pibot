﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SerialClientDetails.cs" company="PiBot">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace PiBot.GUI.ClientDatabase
{
    using System.Globalization;
    using System.Xml.Linq;

    using CmdMessenger.CmdComms;

    public class SerialClientDetails : ClientDetails
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SerialClientDetails"/> class. 
        /// </summary>
        /// <param name="element">
        /// Raw XML representation of the client details.
        /// </param>
        public SerialClientDetails(XElement element)
        {
            this.Port = element.Element("Port").Value;
            this.Baurd = int.Parse(element.Element("Baurd").Value);
        }

        #endregion

        #region Properties

        public override ClientType ClientType
        {
            get { return ClientType.Serial; }
        }

        private string Port { get; set; }

        private int Baurd { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Get a description of the comms;
        /// </summary>
        /// <returns>
        /// A description of the interface.
        /// </returns>
        protected override string GetDescription()
        {
            return string.Format(CultureInfo.InvariantCulture, "Serial, COM = {0}, Baud = {1}", this.Port, this.Baurd);
        }

        public override ICmdComms Create()
        {
            return new SerialCmdClient(this.Port, this.Baurd);
        }

        #endregion
    }
}
