﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TcpClientDetails.cs" company="PiBot">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace PiBot.GUI.ClientDatabase
{
    using System;
    using System.Globalization;
    using System.Xml.Linq;

    using CmdMessenger.CmdComms;

    /// <summary>
    /// A data transfer object for TCP client details.
    /// </summary>
    public class TcpClientDetails : ClientDetails
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TcpClientDetails"/> class.
        /// </summary>
        /// <param name="element">Raw XML representation of the client details.</param>
        public TcpClientDetails(XElement element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }

            this.Address = element.Element("Address").Value;
            this.Port = int.Parse(element.Element("Port").Value);
        }

        public TcpClientDetails(string address, int port)
        {
            this.Address = address;
            this.Port = port;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the client type.
        /// </summary>
        public override ClientType ClientType
        {
            get { return ClientType.Tcp; }
        }

        /// <summary>
        /// Gets the clients address
        /// </summary>
        private string Address { get; set; }

        /// <summary>
        /// Gets the client port number.
        /// </summary>
        private int Port { get; set; }

        #endregion

        /// <summary>
        /// Get a description of the comms;
        /// </summary>
        /// <returns>A description of the interface type.</returns>
        protected override string GetDescription()
        {
            return string.Format(CultureInfo.InvariantCulture, "Address = {0}, Port = {1}", this.Address, this.Port);
        }

        public override ICmdComms Create()
        {
            return new TcpCmdClient(this.Address, this.Port);
        }
    }
}