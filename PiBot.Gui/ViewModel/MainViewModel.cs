﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainViewModel.cs" company="PiBot">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace PiBot.GUI.ViewModel
{
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Threading;
    using System.Windows.Input;

    using CmdMessenger;
    using CmdMessenger.Commands;

    using ControlPad;

    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    using PiBot.Common;
    using PiBot.Common.Commands;
    using PiBot.Common.Logging;
    using PiBot.GUI.ClientDatabase;

    /// <summary>
    /// View model for control pad.
    /// </summary>
    public class MainViewModel : ViewModelBase, IDisposable
    {
        #region Fields

        private readonly CmdFactory factory;
        private readonly CancellationTokenSource connectCancellationToken;
        private CmdMessenger messenger;
        private bool connected;
        private string status;
        private ClientDetails selectedClient;
        private PadDirection lastPadDirection = PadDirection.None;
        private bool disposed;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class. 
        /// </summary>
        public MainViewModel()
        {
            ClientDatabase clientDatabase = new ClientDatabase();
            this.ConnectCommand = new RelayCommand(this.Connect);
            this.DisconnectCommand = new RelayCommand(this.Disconnect);

            this.connectCancellationToken = new CancellationTokenSource();
            this.Connected = false;
            this.factory = new CmdFactory();
            this.Log = new LogVM();
            this.KeyPressedCommand = new RelayCommand<ControlPadEventArgs>(this.KeyPressedExecuted);
            this.Clients = new ObservableCollection<ClientDetails>(clientDatabase.Parse("Clients.xml").ToList());
            
            #if DEBUG

            this.Clients.Add(new TcpClientDetails("127.0.0.1", 5000));

            #endif
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the connect command.
        /// </summary>
        public ICommand ConnectCommand { get; private set; }

        /// <summary>
        /// Gets the disconnect command.
        /// </summary>
        public ICommand DisconnectCommand { get; private set; }

        /// <summary>
        /// Gets the clients
        /// </summary>
        public ObservableCollection<ClientDetails> Clients { get; set; }
        
        /// <summary>
        /// Command called on key pressed
        /// </summary>
        public ICommand KeyPressedCommand { get; private set; }

        public LogVM Log { get; set; }

        public ClientDetails SelectedClient
        {
            get
            {
                return this.selectedClient;
            }

            set 
            { 
                this.selectedClient = value;                
                this.RaisePropertyChanged("SelectedClient");
            }
        }

        public string Status
        {
            get
            {
                return this.status;
            }

            set
            {
                this.status = value;
                this.RaisePropertyChanged("Status");
            }
        }

        public bool Connected
        {
            get
            {
                return this.connected;
            }

            set
            {
                this.connected = value;
                this.RaisePropertyChanged("Connected");
            }
        }

        #endregion

        #region Methods

        private async void Connect()
        {
            if (this.selectedClient != null)
            {
                var c = this.selectedClient.Create();
                this.messenger = new CmdMessenger(c);
                this.Status = string.Format("Trying to connect: {0}", this.selectedClient.Description);
                this.Connected = true;
                this.Status = "Connected";
                this.messenger.Register((int)PiBotCommands.Ping, r => this.messenger.Send(new PingResponse()));
            }         
        }

        private void Disconnect()
        {
            this.messenger.Stop();
            this.Status = "Disconnected";
            this.Connected = false;
        }

        private void KeyPressedExecuted(ControlPadEventArgs controlPadEventArgs)
        {
            // Only trigger if changed.
            if (this.lastPadDirection != controlPadEventArgs.Direction)
            {
                this.lastPadDirection = controlPadEventArgs.Direction;

                switch (controlPadEventArgs.Direction)
                {
                    case PadDirection.Up:
                        this.UpExecuted();
                        break;
                    case PadDirection.Down:
                        this.DownExecuted();
                        break;
                    case PadDirection.Left:
                        this.LeftExecuted();
                        break;
                    case PadDirection.Right:
                        this.RightExecuted();
                        break;
                    case PadDirection.RotateLeft:
                         this.RotateLeftExecuted();
                        break;
                    case PadDirection.RotateRight:
                        this.RotateRightExecuted();
                        break;
                    case PadDirection.Stop:
                        this.StopExecuted();
                        break;
                }
            }
        }

        private void StopExecuted()
        {
            this.Status = "Stopped";
            this.SendCommand(this.factory.CreateSetMotorSpeedMessage(50, 50, 50, 50));         
        }

        private void RotateRightExecuted()
        {
            this.Status = "Rotate right";
            this.SendCommand(this.factory.CreateSetMotorSpeedMessage(0, 100, 0, 100));
        }

        private void RotateLeftExecuted()
        {
            this.Status = "Rotate left";
            this.SendCommand(this.factory.CreateSetMotorSpeedMessage(100, 0, 100, 0));
        }

        private void RightExecuted()
        {
            this.Status = "Move right";
            this.SendCommand(this.factory.CreateSetMotorSpeedMessage(100, 100, 0, 0));
        }

        private void LeftExecuted()
        {
            this.Status = "Move left";
            this.SendCommand(this.factory.CreateSetMotorSpeedMessage(0, 0, 100, 100));
        }

        private void DownExecuted()
        {
            this.Status = "Move down";
            this.SendCommand(this.factory.CreateSetMotorSpeedMessage(100, 100, 100, 100));
        }

        public void UpExecuted()
        {
            if (this.disposed)
            {
                throw new ObjectDisposedException(typeof(MainViewModel).Name);
            }

            this.Status = "Move forwards";
            this.SendCommand(this.factory.CreateSetMotorSpeedMessage(0, 0, 0, 0));
        }

        private void SendCommand(ISendCommand command)
        {
            try
            {
                this.messenger.SendAsync(command);
            }
            catch (Exception ex)
            {
                this.Log.Error(ex.Message);
            }
        }        
        
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    this.connectCancellationToken.Dispose();
                    this.messenger.Dispose();
                }

                this.disposed = true;
            }
        }

        #endregion
    }
}
