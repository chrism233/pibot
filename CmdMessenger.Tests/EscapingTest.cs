﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EscapingTest.cs" company="PiBot">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace CmdMessenger.Tests
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;

    using NUnit.Framework;

    [TestFixture]
    public class EscapingTest
    {
        [Test]
        public void UnescapeParameters()
        {
            var escaping = new Escaping(',', ';', '\\');

            string[] result = escaping.GetUnescapedParameters(Encoding.ASCII.GetBytes("10,He\\,llo"));

            Assert.AreEqual(2, result.Count());
            Assert.AreEqual("10", result[0], "The first parameter should be 10.");
            Assert.AreEqual("He,llo", result[1], @"The second parameter should be ""He,llo"" ");
        }

        [Test]
        public void CanEscapeParameters()
        {
            var escaping = new Escaping(',', ';', '/');

            string result = escaping.EscapeParameters("10", "He,llo");

            Assert.AreEqual("10,He/,llo;", result);
        }

        [Test]
        public void GetCommands()
        {
            var escaping = Escaping.Default;

            IEnumerable<string> commands = escaping.GetCommands("10101010101;101010101");

            Assert.AreEqual(1, commands.Count());
        }

        [Test]
        public void GetCommandsStream()
        {
            var escaping = Escaping.Default;
            var queu = new Queue<byte>();
            Encoding.ASCII.GetBytes("10101010101;101010101;").ToList().ForEach(queu.Enqueue);
            
           // byte[] commands1 = escaping.GetCommand(queu).First();
           // byte[] commands2 = escaping.GetCommand(queu).First();
        }
    }
}
