﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Blocking.cs" company="PiBot">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   Defines the Blocking type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CmdMessenger.Tests
{
    using System;
    using System.Diagnostics;

    public class Blocking
    {
        private readonly Stopwatch stopWatch;
        private readonly int seconds;

        public Blocking(int seconds)
        {
            this.seconds = seconds;
            this.stopWatch = new Stopwatch();
            this.stopWatch.Start();
        }

        public bool Untill(Func<bool> func)
        {
            bool result = true;

            do
            {
                if (this.stopWatch.ElapsedMilliseconds > this.seconds * 1000)
                {
                    result = false;
                    break;
                }
            }
            while (!func());

            return result;
        }
    }
}