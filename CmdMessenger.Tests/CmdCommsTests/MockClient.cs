﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MockClient.cs" company="PiBot">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace CmdMessenger.Tests.CmdCommsTests
{
    using System.IO;
    using System.Text;
    using System.Threading.Tasks;

    public class MockClient : CmdComms.CmdComms
    {
        private readonly MemoryStream stream;

        #region Constructor

        public MockClient()
        {
            this.stream = new MemoryStream();
        }

        #endregion

        #region Properties

        public Stream Stream
        {
            get { return this.stream; }
        }

        #endregion

        #region Methods

        public string StreamString()
        {
            return Encoding.ASCII.GetString(this.stream.ToArray());
        }

        protected override Stream GetStream()
        {
            return this.stream;
        }

        public override Task OpenAsync()
        {
            var tcs = new TaskCompletionSource<bool>();
            tcs.SetResult(true);
            return tcs.Task;
        }

        /// <summary>
        /// Disconnect from the device.
        /// </summary>
        public override void Close()
        {
            this.stream.Close();
        }

        /// <summary>
        /// Get a description of the communications.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// A description of the communications interface.
        /// </returns>
        protected string GetDescription()
        {
            return "Mock";
        }

        #endregion
    }
}
