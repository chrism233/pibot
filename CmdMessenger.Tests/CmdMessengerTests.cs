﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdMessengerTests.cs" company="PiBot">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace CmdMessenger.Tests
{
    using System.Threading.Tasks;

    using global::CmdMessenger.CmdComms;
    using global::CmdMessenger.Commands;
    using global::CmdMessenger.Tests.CmdCommsTests;

    using NUnit.Framework;

    public class MockCmdServer
    {
        private readonly CmdMessenger cmdMessenger;

        public MockCmdServer()
        {
          //  this.cmdMessenger = new CmdMessenger(new TcpCmdServer(5000));
            this.cmdMessenger.Register(
                5,
                c => this.cmdMessenger.Send(new SendCommand(6)));
        }

        public void Start()
        {
            this.cmdMessenger.Start();
        }

        public void Stop()
        {
            this.cmdMessenger.Stop();
        }
    }

    [TestFixture]
    public class CmdMessengerTests
    {
        [Test]
        public void IntegrationTest()
        {
            var cmdServer = new MockCmdServer();
            Task.Factory.StartNew(cmdServer.Start);
            
            var cmdClient = new CmdMessenger(new TcpCmdClient("127.0.0.1", 5000));
            cmdClient.Start();
            IReceivedCommand command = cmdClient.Send(new SendCommand(5, 6));

            cmdClient.Stop();
            cmdServer.Stop();
        }

        [Test]
        public void DataSent_Send_SendsData()
        {
            var client = new MockClient();
            var m = new CmdMessenger(client);
            m.Send(new SendCommand(5));
            Assert.AreEqual("5;", client.StreamString());
        }

        [Ignore]
        [Test]
        public void ResponseHandled_Send_ReturnsResponse()
        {
            var client = new MockClient();
            var m = new CmdMessenger(client);
            m.Start();

            bool responseReceived = false;
            Task.Factory.StartNew(
                () =>
                    {
                        var result = m.Send(new SendCommand(5, 6));
                        Assert.AreEqual(6, result.CommandId);
                        responseReceived = true;
                        m.Stop();
                    });

            m.Send(new SendCommand(6));
            client.Stream.Position = client.Stream.Position - 2;
   
            Assert.IsTrue(TestHelper.Wait(5).Untill(() => responseReceived), "This timed out");
        }

        [Ignore]
        [Test]
        [ExpectedException(typeof(TaskCanceledException))]
        public void ResponseHandled_Send_ThrowsCmdTimeoutEx()
        {
            var client = new MockClient();

            var m = new CmdMessenger(client);
            var result = m.Send(new SendCommand(5, 6));

            TestHelper.Wait(5);
        }

        [Test]
        public void HandleUnidirectionalCommand_Listen_CommandReceived()
        {
            var client = new MockClient();
            var m = new CmdMessenger(client);

            bool commandReceived = false;
            m.Register(6, r => { commandReceived = true; });

            m.Send(new SendCommand(6));
            client.Stream.Position = 0;
            m.Start();

            Assert.IsTrue(TestHelper.Wait(5).Untill(() => commandReceived));

            m.Stop();
        }

        [Ignore]
        [Test]
        [Category("Integration")]
        public void Test()
        {
            var sendMessage = new SendCommand(0, 1);
            sendMessage.AddArguments(true);          // Add a Boolean argument.
            sendMessage.AddArguments("Hello World"); // Add a String argument. 
            sendMessage.AddArguments(5);             // Add am integer argument. 
            Assert.AreEqual("0,1,Hello World,5;", sendMessage.GetCommand()); 

        }
    }
}
