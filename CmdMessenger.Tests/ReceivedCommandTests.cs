﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReceivedCommandTests.cs" company="PiBot">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace CmdMessenger.Tests
{
    using System.Linq;

    using global::CmdMessenger.Commands;

    using NUnit.Framework;

    [TestFixture]
    public class ReceivedCommandTests
    {
        [Test]
        public void Test()
        {
            var cmd = new SendCommand(1, 2);

            cmd.AddArguments(20, 20, 20, 20);

            string[] arguments = cmd.GetCommand().TrimEnd(';').Split(',').ToArray();

            var response = new ReceivedCommand(int.Parse(arguments[0]), arguments.Skip(1).ToArray());

            string temp = cmd.GetCommand();

            Assert.AreEqual("1,20,20,20,20;", temp);

            Assert.AreEqual(20, response.ReadInt32());
            Assert.AreEqual(20, response.ReadInt32());
            Assert.AreEqual(20, response.ReadInt32());
            Assert.AreEqual(20, response.ReadInt32());
        }
    }
}
