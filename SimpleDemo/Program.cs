﻿

namespace SimpleDemo
{
    using System;
    using System.Net;
    using System.Net.Sockets;
    using System.Threading.Tasks;
    using CmdMessenger.CmdComms;
    using CmdMessenger.Commands;

    internal class Program
    {
        private static void Main()
        {
            // ************** Start CmdMessenger server ****************
            Task.Factory.StartNew(
                () =>
                {
                    var listener = new TcpListener(IPAddress.Loopback, 5000);
                    listener.Start();
                    var cmdServer = new CmdMessenger.CmdMessenger(new TcpCmdClient(listener.AcceptTcpClient()));
                    cmdServer.Register(0, r => cmdServer.SendAsync(new SendCommand(1)));
                    cmdServer.Start();
                    Console.WriteLine("Server started.");
                });

            // ************** End CmdMessenger server ****************

            // Create an instance of CmdMessenger and provide an implementation of the transport layer.
            var cmdClient = new CmdMessenger.CmdMessenger(new TcpCmdClient("127.0.0.1", 5000));
            // Register one or more command handlers.
            cmdClient.Register(1, r => Console.WriteLine("Response received"));
            // Open the connection and begin processing incoming commands.
            cmdClient.Start();

            while (Console.ReadLine() != "x")
            {
                // Send a simple command with no arguments.
                cmdClient.Send(new SendCommand(0, 1));
            }
        }
    }
}