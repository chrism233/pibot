﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PadDirection.cs" company="PiBot">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ControlPad
{
    /// <summary>
    /// The possible pad directions.
    /// </summary>
    public enum PadDirection
    {
        /// <summary>
        /// Move Up
        /// </summary>
        Up,

        /// <summary>
        /// Move Down
        /// </summary>
        Down,

        /// <summary>
        /// Move left
        /// </summary>
        Left,

        /// <summary>
        /// Move right 
        /// </summary>
        Right,

        /// <summary>
        /// Move left
        /// </summary>
        RotateLeft,

        /// <summary>
        /// Move right
        /// </summary>
        RotateRight,

        /// <summary>
        /// Stop or break
        /// </summary>
        Stop,

        /// <summary>
        /// Do nothing 
        /// </summary>
        None
    }
}
