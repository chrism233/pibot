﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ControlPad.cs" company="PiBot">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ControlPad
{
    using System;
    using System.ComponentModel;
    using System.Diagnostics.CodeAnalysis;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media.Imaging;

    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// A multi directional keypad control.
    /// </summary>
    public class ControlPad : Control, INotifyPropertyChanged
    {
        #region Fields

        /// <summary>
        /// The key pressed event.
        /// </summary>
        private static readonly RoutedEvent KeyPressedEvent = EventManager.RegisterRoutedEvent(
            "KeyPressed",
            RoutingStrategy.Bubble,
                typeof(RoutedEventHandler),
            typeof(ControlPad));

        private readonly RelayCommand rotateRightCommand;
        private readonly RelayCommand rotateLeftCommand;

        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Reviewed. Suppression is OK here.")]
        private readonly RelayCommand upCommand;
        private readonly RelayCommand downCommand;
        private readonly RelayCommand leftCommand;
        private readonly RelayCommand rightCommand;
        private readonly RelayCommand stopCommand;
        private BitmapSource circleLeftImage;
        private BitmapSource circleRightImage;

        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Reviewed. Suppression is OK here.")]
        private BitmapSource upImage;
        private BitmapSource downImage;
        private BitmapSource leftImage;
        private BitmapSource rightImage;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes static members of the <see cref="ControlPad"/> class.
        /// </summary>
        static ControlPad()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ControlPad), new FrameworkPropertyMetadata(typeof(ControlPad)));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ControlPad"/> class.
        /// </summary>
        public ControlPad()
        {
            this.upCommand = new RelayCommand(this.UpExecuted);
            this.downCommand = new RelayCommand(this.DownExecuted);
            this.leftCommand = new RelayCommand(this.LeftExecuted);
            this.rightCommand = new RelayCommand(this.RightExecuted);
            this.stopCommand = new RelayCommand(this.StopExecuted);
            this.rotateLeftCommand = new RelayCommand(this.RotateLeftExecuted);
            this.rotateRightCommand = new RelayCommand(this.RotateRightExecuted);

            this.StopExecuted();

            this.PreviewKeyUp += this.ControlPad_PreviewKeyUp;
            this.KeyBoardKeyUp += this.ControlPad_KeyBoardKeyUp;
            this.IsEnabledChanged += this.ControlPadIsEnabledChanged;
        }

        #endregion

        #region Events

        /// <summary>
        /// The key pressed.
        /// </summary>
        public event RoutedEventHandler KeyPressed
        {
            add { this.AddHandler(KeyPressedEvent, value); }
            remove { this.RemoveHandler(KeyPressedEvent, value); }
        }

        /// <summary>
        /// The key board key up.
        /// </summary>
        public event RoutedEventHandler KeyBoardKeyUp
        {
            add { this.AddHandler(Keyboard.PreviewKeyUpEvent, value); }
            remove { this.AddHandler(Keyboard.PreviewKeyUpEvent, value); }
        }

        /// <summary>
        /// Occurs when a property changes in the <see cref="ControlPad"/>
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        
        #endregion

        #region Properties

        /// <summary>
        /// Gets the rotate right command.
        /// </summary>
        public ICommand RotateRightCommand
        {
            get { return this.rotateRightCommand; }
        }

        /// <summary>
        /// Gets the rotate left command.
        /// </summary>
        public ICommand RotateLeftCommand
        {
            get { return this.rotateLeftCommand; }
        }

        /// <summary>
        /// Gets the up command.
        /// </summary>
        public ICommand UpCommand
        {
            get { return this.upCommand; }
        }

        /// <summary>
        /// Gets the down command.
        /// </summary>
        public ICommand DownCommand
        {
            get { return this.downCommand; }
        }

        /// <summary>
        /// Gets the left command.
        /// </summary>
        public ICommand LeftCommand
        {
            get { return this.leftCommand; }
        }

        /// <summary>
        /// Gets the right command.
        /// </summary>
        public ICommand RightCommand
        {
            get { return this.rightCommand; }
        }

        /// <summary>
        /// Gets the stop command.
        /// </summary>
        public ICommand StopCommand
        {
            get { return this.stopCommand; }
        }

        /// <summary>
        /// Gets or sets the circle left image.
        /// </summary>
        public BitmapSource CircleLeftImage
        {
            get
            {
                return this.circleLeftImage;
            }

            set
            {
                this.circleLeftImage = value;
                this.RaisePropertyChanged("CircleLeftImage");
            }
        }

        /// <summary>
        /// Gets or sets the circle right image.
        /// </summary>
        public BitmapSource CircleRightImage
        {
            get
            {
                return this.circleRightImage;
            }

            set
            {
                this.circleRightImage = value;
                this.RaisePropertyChanged("CircleRightImage");
            }
        }

        /// <summary>
        /// Gets or sets the up image.
        /// </summary>
        public BitmapSource UpImage
        {
            get
            {
                return this.upImage;
            }

            set
            {
                this.upImage = value;
                this.RaisePropertyChanged("UpImage");
            }
        }

        /// <summary>
        /// Gets or sets the down image.
        /// </summary>
        public BitmapSource DownImage
        {
            get
            {
                return this.downImage;
            }

            set
            {
                this.downImage = value;
                this.RaisePropertyChanged("DownImage");
            }
        }

        /// <summary>
        /// Gets or sets the left image.
        /// </summary>
        public BitmapSource LeftImage
        {
            get
            {
                return this.leftImage;
            }

            set
            {
                this.leftImage = value;
                this.RaisePropertyChanged("LeftImage");
            }
        }

        /// <summary>
        /// Gets or sets the right image.
        /// </summary>
        public BitmapSource RightImage
        {
            get
            {
                return this.rightImage;
            }

            set
            {
                this.rightImage = value;
                this.RaisePropertyChanged("RightImage");
            }
        }

        #endregion

        private void ControlPadIsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue)
            {
                CommandManager.RegisterClassInputBinding(typeof(UIElement), new InputBinding(this.UpCommand, new KeyGesture(Key.Up)));
                CommandManager.RegisterClassInputBinding(typeof(UIElement), new InputBinding(this.DownCommand, new KeyGesture(Key.Down)));
                CommandManager.RegisterClassInputBinding(typeof(UIElement), new InputBinding(this.LeftCommand, new KeyGesture(Key.Left)));
                CommandManager.RegisterClassInputBinding(typeof(UIElement), new InputBinding(this.RightCommand, new KeyGesture(Key.Right)));

                CommandManager.RegisterClassInputBinding(typeof(UIElement), new InputBinding(this.RotateLeftCommand, new KeyGesture(Key.Home)));
                CommandManager.RegisterClassInputBinding(typeof(UIElement), new InputBinding(this.RotateRightCommand, new KeyGesture(Key.PageUp)));
            }
            else
            {
                CommandManager.RegisterClassInputBinding(typeof(UIElement), new InputBinding(ApplicationCommands.NotACommand, new KeyGesture(Key.Up)));
                CommandManager.RegisterClassInputBinding(typeof(UIElement), new InputBinding(ApplicationCommands.NotACommand, new KeyGesture(Key.Down)));
                CommandManager.RegisterClassInputBinding(typeof(UIElement), new InputBinding(ApplicationCommands.NotACommand, new KeyGesture(Key.Left)));
                CommandManager.RegisterClassInputBinding(typeof(UIElement), new InputBinding(ApplicationCommands.NotACommand, new KeyGesture(Key.Right)));

                CommandManager.RegisterClassInputBinding(typeof(UIElement), new InputBinding(ApplicationCommands.NotACommand, new KeyGesture(Key.Home)));
                CommandManager.RegisterClassInputBinding(typeof(UIElement), new InputBinding(ApplicationCommands.NotACommand, new KeyGesture(Key.PageUp)));
            }
        }

        private void ControlPad_KeyBoardKeyUp(object sender, RoutedEventArgs e)
        {
            this.StopExecuted();
        }

        private void ControlPad_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            this.StopExecuted();
        }

        public void RaisePropertyChanged(string name)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        private void StopExecuted()
        {
            this.CircleLeftImage = new CroppedBitmap(new BitmapImage(new Uri("pack://application:,,,/Images/CircleArrows2.png")), new Int32Rect(0, 92, 95, 90));
            this.CircleRightImage = new CroppedBitmap(new BitmapImage(new Uri("pack://application:,,,/Images/CircleArrows2.png")), new Int32Rect(97, 92, 95, 90));

            this.UpImage = new CroppedBitmap(new BitmapImage(new Uri("pack://application:,,,/Images/tango_arrows.jpg")), new Int32Rect(261, 0, 280, 270));
            this.DownImage = new CroppedBitmap(new BitmapImage(new Uri("pack://application:,,,/Images/tango_arrows.jpg")), new Int32Rect(261, 480, 280, 270));
            this.LeftImage = new CroppedBitmap(new BitmapImage(new Uri("pack://application:,,,/Images/tango_arrows.jpg")), new Int32Rect(0, 256, 280, 270));
            this.RightImage = new CroppedBitmap(new BitmapImage(new Uri("pack://application:,,,/Images/tango_arrows.jpg")), new Int32Rect(520, 256, 280, 270));

            this.RaiseEvent(new ControlPadEventArgs(KeyPressedEvent, PadDirection.Stop));
        }

        private void UpExecuted()
        {
            this.UpImage = new CroppedBitmap(new BitmapImage(new Uri("pack://application:,,,/Images/tango_arrows_blue.png")), new Int32Rect(261, 0, 280, 270));
            this.RaiseEvent(new ControlPadEventArgs(KeyPressedEvent, PadDirection.Up));
        }

        private void DownExecuted()
        {
            this.DownImage = new CroppedBitmap(new BitmapImage(new Uri("pack://application:,,,/Images/tango_arrows_blue.png")), new Int32Rect(261, 480, 280, 270));
            this.RaiseEvent(new ControlPadEventArgs(KeyPressedEvent, PadDirection.Down));
        }

        private void LeftExecuted()
        {
            this.LeftImage = new CroppedBitmap(new BitmapImage(new Uri("pack://application:,,,/Images/tango_arrows_blue.png")), new Int32Rect(0, 256, 280, 270));
            this.RaiseEvent(new ControlPadEventArgs(KeyPressedEvent, PadDirection.Left));
        }

        private void RightExecuted()
        {
            this.RightImage = new CroppedBitmap(new BitmapImage(new Uri("pack://application:,,,/Images/tango_arrows_blue.png")), new Int32Rect(520, 256, 280, 270));
            this.RaiseEvent(new ControlPadEventArgs(KeyPressedEvent, PadDirection.Right));
        }

        private void RotateRightExecuted()
        {
            this.CircleRightImage = new CroppedBitmap(new BitmapImage(new Uri("pack://application:,,,/Images/CircleArrows2.png")), new Int32Rect(97, 1, 95, 90));
            this.RaiseEvent(new ControlPadEventArgs(KeyPressedEvent, PadDirection.RotateRight));
        }

        private void RotateLeftExecuted()
        {
            this.CircleLeftImage = new CroppedBitmap(new BitmapImage(new Uri("pack://application:,,,/Images/CircleArrows2.png")), new Int32Rect(0, 1, 95, 90));
            this.RaiseEvent(new ControlPadEventArgs(KeyPressedEvent, PadDirection.RotateLeft));
        }

        public override void OnApplyTemplate()
        {
            var cmdUp = GetTemplateChild("cmdUp") as Button;
            Keyboard.Focus(cmdUp);
            base.OnApplyTemplate();
        }
    }
}
