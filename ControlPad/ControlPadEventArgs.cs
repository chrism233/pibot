﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ControlPadEventArgs.cs" company="PiBot">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ControlPad
{
    using System.Windows;

    /// <summary>
    /// Event arguments for the control pas.
    /// </summary>
    public class ControlPadEventArgs : RoutedEventArgs
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ControlPadEventArgs"/> class.
        /// </summary>
        /// <param name="keyPressed">The key pressed routed event.</param>
        /// <param name="direction">The current pad direction.</param>
        public ControlPadEventArgs(RoutedEvent keyPressed, PadDirection direction)
            : base(keyPressed)
        {
            this.Direction = direction;
        }

        #endregion

        #region Properties 

        /// <summary>
        /// Gets the pad direction
        /// </summary>
        public PadDirection Direction { get; private set; }

        #endregion
    }
}