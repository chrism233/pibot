﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using PiBot.ClientDatabase;

namespace PiBot.Tests
{
    [TestFixture]
    public class ClientDatabaseTests
    {
        [Test]
        public void Test_Parse_ReturnsResults()
        {
            var database = new Database();

            var result = database.Parse("Clients.xml");

            Assert.AreEqual(2, result.Count());
        }
    }
}
