﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IReceivedCommand.cs" company="PiBot">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace CmdMessenger.Commands
{
    public interface IReceivedCommand
    {
        /// <summary>
        /// Gets the command ID.
        /// </summary>
        int CommandId { get; }

        /// <summary>
        /// Read <code>short</code>
        /// </summary>
        /// <returns>The <code>short</code> value.</returns>
        short ReadInt16();

        /// <summary>
        /// Read <code>bool</code>
        /// </summary>
        /// <returns>The <code>bool</code> value.</returns>
        bool ReadBool();

        /// <summary>
        /// Read <code>int</code>
        /// </summary>
        /// <returns>The <code>int</code> value.</returns>
        int ReadInt32();

        /// <summary>
        /// Read <code>UInt32</code>
        /// </summary>
        /// <returns>The <code>uint</code> value.</returns>
        uint ReadUInt32();

        /// <summary>
        /// Read the current argument as string.
        /// </summary>
        /// <returns>The <code>sting</code> value.</returns>
        string ReadString();
    }
}