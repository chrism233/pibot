﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ISendCommand.cs" company="PiBot">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace CmdMessenger.Commands
{
    using System.Collections.Generic;

    public interface ISendCommand
    {
        #region Properties

        /// <summary>
        /// Gets the command arguments.
        /// </summary>
        IEnumerable<string> Arguments { get; }

        /// <summary>
        /// Gets the command id
        /// </summary>
        int CommandId { get; }

        /// <summary>
        /// Gets the Ack command Id
        /// </summary>
        int? AckCommandId { get; }

        /// <summary>
        /// Gets the timeout period.
        /// </summary>
        int Timeout { get; }

        #endregion

        #region Methods

        /// <summary>
        /// Get the command.
        /// </summary>
        /// <returns>A raw string representation of the command.</returns>
        string GetCommand();

        /// <summary>
        /// Add a command argument.
        /// </summary>
        /// <typeparam name="T">The argument type.</typeparam>
        /// <param name="argument">The argument to add.</param>
        void AddArguments<T>(T argument);

        /// <summary>
        /// Add arguments.
        /// </summary>
        /// <typeparam name="T">The argument type.</typeparam>
        /// <param name="arguments">The arguments to add.</param>
        void AddArguments<T>(params T[] arguments);

        /// <summary>
        /// Add <code>bool</code> argument.
        /// </summary>
        /// <param name="argument">The boolean argument to add.</param>
        void AddArguments(bool argument);

        /// <summary>
        /// Add arguments.
        /// </summary>
        /// <param name="arguments">The boolean arguments to add.</param>
        void AddArguments(params bool[] arguments);

        #endregion
    }
}