// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICmdComms.cs" company="PiBot">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace CmdMessenger.CmdComms
{
    using System.Threading;
    using System.Threading.Tasks;

    using global::CmdMessenger.Commands;

    /// <summary>
    /// An interface for interface types.
    /// </summary>
    public interface ICmdComms
    {
        #region Methods

        /// <summary>
        /// Connect to the device.
        /// </summary>
        /// <returns><code>true</code> if the connection is open.</returns>
        Task OpenAsync();

        /// <summary>
        /// Disconnect from the device.
        /// </summary>
        void Close();

        /// <summary>
        /// Writes a parameter to the serial port.
        /// </summary>
        /// <param name="command">The command to send.</param>
        void Send(ISendCommand command);

        /// <summary>
        /// Asynchronously, writes a command to the stream.
        /// </summary>
        /// <param name="command">The command to send.</param>
        /// <returns>A task indicating when the send has completed.</returns>
        Task SendAsync(ISendCommand command);

        /// <summary>
        /// Reads a command from the stream.
        /// </summary>
        /// <returns></returns>
        IReceivedCommand Read(CancellationToken token);

        /// <summary>
        /// Asynchronously, reads a command from the stream.  
        /// </summary>
        /// <returns>The command read from the stream.</returns>
        Task<IReceivedCommand> ReadAsync(CancellationToken token);

        #endregion
    }
}