// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IEscaping.cs" company="PiBot">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace CmdMessenger
{
    using System.Collections.Generic;
    using System.IO;

    public interface IEscaping
    {
        /// <summary>
        /// Get the un-escaped parameters  
        /// </summary>
        /// <param name="line">
        /// The line to parse.
        /// </param>
        /// <returns>
        /// A collection of the unescaped parameters.
        /// </returns>
        string[] GetUnescapedParameters(byte[] line);

        /// <summary>
        /// Pack the message ready for sending.
        /// </summary>
        /// <param name="input">The parameters to escape.</param>
        /// <returns>The escaped parameters.</returns>
        string EscapeParameters(params string[] input);

        /// <summary>
        /// Read off the first command.
        /// </summary>
        /// <param name="temp">
        /// The string containing multiple commands.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// A enumerable of commands.
        /// </returns>
        IEnumerable<string> GetCommands(string temp);

        IEnumerable<byte[]> GetCommand(Stream stream);
    }
}