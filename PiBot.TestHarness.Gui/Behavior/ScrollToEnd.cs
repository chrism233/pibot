﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ScrollToEnd.cs" company="PiBot">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The auto scroll behavior.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace PiBot.TestHarness.Gui.Behavior
{
    using System;
    using System.Windows.Controls;
    using System.Windows.Interactivity;

    /// <summary>
    /// The auto scroll behavior.
    /// </summary>
    public class AutoScrollBehavior : Behavior<ListView>
    {
        #region Fields

        /// <summary>
        /// The _scroll viewer.
        /// </summary>
        private ListView scrollViewer;

        #endregion

        #region Methods

        /// <summary>
        /// The on attached.
        /// </summary>
        protected override void OnAttached()
        {
            base.OnAttached();

            this.scrollViewer = this.AssociatedObject;
            this.scrollViewer.LayoutUpdated += this.ScrollViewerLayoutUpdated;
        }

        /// <summary>
        /// The on detaching.
        /// </summary>
        protected override void OnDetaching()
        {
            base.OnDetaching();

            if (this.scrollViewer != null)
            {
                this.scrollViewer.LayoutUpdated -= this.ScrollViewerLayoutUpdated;
            }
        }

        /// <summary>
        /// The _scroll viewer_ layout updated.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void ScrollViewerLayoutUpdated(object sender, EventArgs e)
        {
            if (this.AssociatedObject.Items.Count > 0)
            {
                if (!AssociatedObject.IsMouseOver)
                {
                    this.AssociatedObject.ScrollIntoView(this.AssociatedObject.Items[this.AssociatedObject.Items.Count - 1]);
                }
            }
        }

        #endregion
    }
}