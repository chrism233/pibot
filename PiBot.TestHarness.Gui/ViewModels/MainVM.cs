﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainVM.cs" company="PiBot">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The main vm.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace PiBot.TestHarness.Gui.ViewModels
{
    using System.ComponentModel.DataAnnotations;
    using System.Windows.Input;

    using CmdMessenger;

    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    using PiBot;
    using PiBot.Common;
    using PiBot.Common.Commands;
    using PiBot.Common.Logging;
    using PiBot.TestHarness;

    /// <summary>
    /// The main VM.
    /// </summary>
    public class MainVM : ViewModelBase
    {
        #region Fields

        /// <summary>
        /// The logger instance.
        /// </summary>
        private readonly LogVM log = new LogVM();

        /// <summary>
        /// The server.
        /// </summary>
        private CmdTcpServer server;

        /// <summary>
        /// The connected.
        /// </summary>
        private bool connected;

        // The port to listen on.
        private int port;

        /// <summary>
        /// The number of connected clients.
        /// </summary>
        private int connectedClients;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM" /> class.
        /// </summary>
        public MainVM()
        {
            this.server = new CmdTcpServer(this.Log);
            this.server.ClientConnectedEvent += this.ServerClientConnectedEvent;
            this.server.ClientDisconnectedEvent += server_ClientDisconnectedEvent;
            this.StartCommand = new RelayCommand(this.Connect);
            this.Log.Info("Waiting to connect.");
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the start command
        /// </summary>
        public ICommand StartCommand { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether connected.
        /// </summary>
        public bool Connected
        {
            get
            {
                return this.connected;
            }

            set
            {
                this.connected = value;
                this.RaisePropertyChanged("Connected");
            }
        }

        /// <summary>
        /// Gets the received.
        /// </summary>
        public LogVM Log
        {
            get
            {
                return this.log;
            }
        }
        
        /// <summary>
        /// Gets or sets the number of connected clients.
        /// </summary>
        public int ConnectedClients
        {
            get
            {
                return this.connectedClients;
            }

            set
            {
                this.connectedClients = value;
                this.RaisePropertyChanged("ConnectedClients");
                this.RaisePropertyChanged("ClientsConnected");
            }
        }

        /// <summary>
        /// Gets the client connected state.
        /// </summary>
        public bool ClientsConnected
        {
            get
            {
                return this.ConnectedClients > 0;
            }
        }

        [Required]
        public int Port
        {
            get
            {
                return this.port;
            }

            set
            {
                this.port = value;
                this.RaisePropertyChanged("Port");
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The connect.
        /// </summary>
        private void Connect()
        {
            this.server.Start(this.Port);
            this.RegisterListeners();
            this.Connected = true;
            Log.Info("Connected");
        }
        
        /// <summary>
        /// The register listeners.
        /// </summary>
        private void RegisterListeners()
        {
            this.server.Register((int)PiBotCommands.SetMotorSpeed, r => this.HandleSetMotorSpeed(new SetMotorSpeed(r)));
        }

        /// <summary>
        /// The handle motor speed.
        /// </summary>
        /// <param name="command">
        /// The command.
        /// </param>
        private void HandleSetMotorSpeed(SetMotorSpeed command)
        {
            // this.server.Send(new SendCommand(6));
            this.Log.Output("SendMotorSpeed");

            string description =
                string.Format(
                    "front left {0,-2}, front right {1,-2}, rear left {2,-2}, rear right {3,-2}", 
                    command.LeftSpeedF, 
                    command.RightSpeedF, 
                    command.LeftSpeedR, 
                    command.RightSpeedR);
            this.Log.Input(description);
        }

        /// <summary>
        /// Handle the on client connected event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="cmdMessenger"></param>
        private void ServerClientConnectedEvent(object sender, CmdMessenger cmdMessenger)
        {
            this.ConnectedClients += 1;
        }

        /// <summary>
        /// Handle the on client disconnected event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void server_ClientDisconnectedEvent(object sender, System.EventArgs e)
        {
            this.ConnectedClients -= 1;
        }

        #endregion
    }
}