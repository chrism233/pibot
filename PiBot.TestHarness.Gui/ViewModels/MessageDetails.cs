// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MessageDetails.cs" company="PiBot">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The message details.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace PiBot.TestHarness.Gui.ViewModels
{
    /// <summary>
    /// The message details.
    /// </summary>
    public class MessageDetails
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageDetails"/> class.
        /// </summary>
        /// <param name="messageDirection">
        /// The message direction.
        /// </param>
        /// <param name="messageDescription">
        /// The message description.
        /// </param>
        public MessageDetails(MessageDirection messageDirection, string messageDescription)
        {
            this.Direction = messageDirection;
            this.Description = messageDescription;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the message description.
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        ///     Gets the message direction.
        /// </summary>
        public MessageDirection Direction { get; private set; }

        #endregion
    }
}